package com.example.contactsmaterial;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

public class ContactAdapter extends ArrayAdapter<Contact> {
    private LayoutInflater inflater;
    private int layout;
    private List<Contact> contacts;

    public ContactAdapter(@NonNull Context context, int resource, List<Contact> contacts) {
        super(context, resource, contacts);
        this.contacts = contacts;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = inflater.inflate(this.layout, parent, false);

        TextView avatarView = view.findViewById(R.id.avatar);
        TextView nameView = view.findViewById(R.id.fullName);
        TextView phoneView = view.findViewById(R.id.phoneNumber);
        final ImageButton favoriteContact = view.findViewById(R.id.favorites);

        final Contact contact = contacts.get(position);

        avatarView.setText(contact.getAvatar());
        nameView.setText(contact.getName());
        phoneView.setText(contact.getPhoneNumber());

        favoriteContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("ContactAdapter ", "Click on start!!!");
                Log.i("ContactAdapter ", contact.getName());
                contact.setFavorite(!contact.isFavorite());
                if (contact.isFavorite()) {
                    favoriteContact.setBackgroundResource(R.drawable.ic_star_24px);
                } else {
                    favoriteContact.setBackgroundResource(R.drawable.ic_star_outline_24px);
                }

            }
        });

        return view;
    }
}
