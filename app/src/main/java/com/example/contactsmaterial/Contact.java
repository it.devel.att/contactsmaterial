package com.example.contactsmaterial;

import java.io.Serializable;

public class Contact implements Serializable {
    private String avatar;
    private String name;
    private String phoneNumber;
    private String email;
    private String webAddress;
    private boolean isFavorite;

    public Contact(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.avatar = String.valueOf(name.charAt(0));
    }

    public Contact(String name, String phoneNumber, String email) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.avatar = String.valueOf(name.charAt(0));
        this.email = email;
    }

    public Contact(String avatar, String name, String phoneNumber, String email, String webAddress) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.avatar = avatar;
        this.email = email;
        this.webAddress = webAddress;
    }

    public Contact(String name, String phoneNumber, String email, String webAddress) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.webAddress = webAddress;
        this.avatar = String.valueOf(name.charAt(0));
    }

    public String getAvatar() {
        return avatar;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getWebAddress() {
        return webAddress;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }
}
