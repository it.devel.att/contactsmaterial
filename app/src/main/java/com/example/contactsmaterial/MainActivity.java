package com.example.contactsmaterial;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private final String TAG = this.getClass().getSimpleName();
    int CREATE_NEW_CONTACT = 1;
    int SHOW_CONTACT = 2;

    private List<Contact> contacts = new ArrayList<>();

    private ListView contactsList;
    private ContactAdapter contactAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CreateContactActivity.class);
                startActivityForResult(intent, CREATE_NEW_CONTACT);

            }
        });

        generateContacts();

        contactsList = findViewById(R.id.contactsList);

        contactAdapter = new ContactAdapter(this, R.layout.contact_row, contacts);
        contactsList.setAdapter(contactAdapter);
        contactsList.setOnItemClickListener(this);
    }

    private void generateContacts() {
        contacts.add(new Contact("Валера Васичкин", "+996 552 123456"));
        contacts.add(new Contact("Антон Кабулов", "+996 774 987456"));
        contacts.add(new Contact("Принтер Рондонт", "+996 552 321456"));
        contacts.add(new Contact("Агонь Валера", "+996 772 321789"));
        contacts.add(new Contact("Король Артур", "+996 555 147852"));
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.i(TAG, "public void onItemClick");
        Contact selectedContact = (Contact) parent.getItemAtPosition(position);
        Intent intent = new Intent(this, ShowContactActivity.class);

        intent.putExtra(Contact.class.getSimpleName(), selectedContact);
        startActivityForResult(intent, SHOW_CONTACT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == CREATE_NEW_CONTACT && resultCode == RESULT_OK) {
            Log.i(TAG, "Return data from create contact activity");
            if (data != null) {
                Contact newContact = (Contact) data.getSerializableExtra(Contact.class.getSimpleName());
                Log.i(TAG, "contactName " + newContact.getName());
                Log.i(TAG, "contactPhone " + newContact.getPhoneNumber());
                Log.i(TAG, "contactMail " + newContact.getEmail());
                Log.i(TAG, "contactWebSite " + newContact.getWebAddress());
                contacts.add(newContact);
                contactAdapter.notifyDataSetChanged();
            }
        }
    }
}
