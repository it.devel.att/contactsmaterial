package com.example.contactsmaterial;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.appbar.AppBarLayout;


public class ShowContactActivity extends AppCompatActivity implements View.OnClickListener {
    private final String TAG = this.getClass().getSimpleName();

    private ImageButton buttonBack;
    private ImageButton favoriteButton;
    private ConstraintLayout shareContactLayout;

    private CardView emailCard;
    private CardView webAddressCard;

    private ImageView sendMessageIcon;

    private TextView contactNameView;
    private TextView contactPhoneView;
    private TextView contactEmailView;
    private TextView contactWebAddressView;

    private Contact contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_contact);
        Toolbar toolbar = findViewById(R.id.toolbar);
        AppBarLayout appBarLayout = findViewById(R.id.appBarLayout);
        appBarLayout.bringToFront();
        toolbar.setTitle("");

        buttonBack = findViewById(R.id.buttonBack);
        buttonBack.setOnClickListener(this);

        favoriteButton = findViewById(R.id.buttonFavorite);


        emailCard = findViewById(R.id.emailCard);
        webAddressCard = findViewById(R.id.webAddressCard);

        sendMessageIcon = findViewById(R.id.messageIcon);
        sendMessageIcon.setOnClickListener(this);

        contactNameView = findViewById(R.id.contactName);
        contactPhoneView = findViewById(R.id.phoneNumber);
        contactEmailView = findViewById(R.id.contactEmail);
        contactWebAddressView = findViewById(R.id.contactWebAddress);

        shareContactLayout = findViewById(R.id.shareContactLayout);
        shareContactLayout.setOnClickListener(this);

        setSupportActionBar(toolbar);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            setContactDataToView(extras);
        }
    }

    private void setContactDataToView(Bundle extras) {
        contact = (Contact) extras.getSerializable(Contact.class.getSimpleName());

        Log.i(TAG, "contactName " + contact.getName());
        Log.i(TAG, "contactPhone " + contact.getPhoneNumber());
        Log.i(TAG, "contactEmail " + contact.getEmail());
        Log.i(TAG, "contactWebAddress " + contact.getWebAddress());


        contactNameView.setText(contact.getName());
        contactPhoneView.setText(contact.getPhoneNumber());
        contactEmailView.setText(contact.getEmail());
        contactWebAddressView.setText(contact.getWebAddress());

        if (contact.getEmail() == null || contact.getEmail().isEmpty()) {
            emailCard.setVisibility(View.GONE);
        }
        if (contact.getWebAddress() == null || contact.getWebAddress().isEmpty()) {
            webAddressCard.setVisibility(View.GONE);
        }

        if (contact.isFavorite()) {
            favoriteButton.setBackgroundResource(R.drawable.ic_star_24px);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonBack:
                finish();
                break;
            case R.id.messageIcon:
                Intent smsIntent = new Intent(Intent.ACTION_SENDTO);
                smsIntent.setData(Uri.parse("smsto:" + Uri.encode(contact.getPhoneNumber())));
                startActivity(smsIntent);
                break;
            case R.id.shareContactLayout:
                Intent shareContactIntent = new Intent(Intent.ACTION_SEND);
                shareContactIntent.setType("text/plain");
                shareContactIntent.putExtra(Intent.EXTRA_TEXT, contact.getPhoneNumber());
                startActivity(shareContactIntent);
                break;
        }
    }
}
