package com.example.contactsmaterial;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class CreateContactActivity extends AppCompatActivity implements View.OnClickListener {
    private final String TAG = this.getClass().getSimpleName();

    public ImageButton closeActivity;
    public Button saveContact;

    public TextInputLayout contactNameLayout;
    public TextInputLayout contactPhoneLayout;

    public TextInputEditText contactName;
    public TextInputEditText contactSurname;
    public TextInputEditText contactPhone;
    public TextInputEditText contactMail;
    public TextInputEditText contactWebSite;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_contact);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        closeActivity = findViewById(R.id.closeContactCreateActivity);
        saveContact = findViewById(R.id.saveNewContact);

        closeActivity.setOnClickListener(this);
        saveContact.setOnClickListener(this);

        contactNameLayout = findViewById(R.id.contactNameLayout);
        contactPhoneLayout = findViewById(R.id.contactPhoneLayout);

        contactName = findViewById(R.id.contactName);
        contactSurname = findViewById(R.id.contactSurname);
        contactPhone = findViewById(R.id.contactPhone);
        contactMail = findViewById(R.id.contactMail);
        contactWebSite = findViewById(R.id.contactWebSite);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.closeContactCreateActivity:
                Log.i(TAG, "Click on close button");
                finish();
                break;
            case R.id.saveNewContact:
                Log.i(TAG, "Click on save button");
                Intent data = new Intent();
                String contactFullName = String.format("%s %s", contactName.getText().toString(), contactSurname.getText().toString());

                Contact contact = new Contact(
                        contactFullName,
                        contactPhone.getText().toString(),
                        contactMail.getText().toString(),
                        contactWebSite.getText().toString()
                );
                data.putExtra(Contact.class.getSimpleName(), contact);


                if (requiredFieldsNotEmpty()) {
                    setResult(RESULT_OK, data);
                    finish();
                }
                break;
        }

    }

    private boolean requiredFieldsNotEmpty() {
        boolean notErrors = true;

        String name = contactName.getText().toString();
        String phone = contactPhone.getText().toString();

        if (name == null || name.isEmpty()) {
            contactNameLayout.setError("Имя обязательно к заполнению");
            notErrors = false;
        }
        if (phone == null || phone.isEmpty()) {
            contactPhoneLayout.setError("Телефон обязателен к заполнению");
            notErrors = false;
        }
        return notErrors;
    }
}
